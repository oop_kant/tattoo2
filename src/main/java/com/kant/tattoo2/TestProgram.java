/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.tattoo2;

/**
 *
 * @author MSI
 */
public class TestProgram {
    public static void main(String[] args) {
        ShopTattoo changkant = new ShopTattoo(1);  //สร้าง object ที่มีชื่อว่า changkant และกำหนดค่าในตัวแปร selectartist เป็น 1 เพื่อเลือกช่าง
        ShopTattoo changton = new ShopTattoo(2); //สร้าง object ที่มีชื่อว่า changton และกำหนดค่าในตัวแปร selectartist เป็น 2 เพื่อเลือกช่าง
        
        changkant.details(50, 5,"FR"); //กำหนดค่าของตัวแปร size , color และ nationality ที่อยู่ใน object changkant ให้มีค่าเป็น 50 , 5 และ "FR" 
        changton.details(20, 2, "TH"); //กำหนดค่าของตัวแปร size , color และ nationality ที่อยู่ใน object changkant ให้มีค่าเป็น 20 , 2 และ "TH" 
        
        System.out.println("รายได้ของช่างกานต์เท่ากับ " +changkant.income + " บาท"); //แสดงรายได้ของช่างกานต์
        System.out.println("รายได้ของช่างต้นเท่ากับ " +changton.income + " บาท"); //แสดงรายได้ของช่างต้น
    }
}
