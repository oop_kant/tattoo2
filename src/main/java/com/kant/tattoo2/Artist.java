/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.tattoo2;

/**
 *
 * @author MSI
 */
public class Artist {
    //proteced เพื่อให้คลาสลูก ใช้ตัวแปรในคลาสแม่ได้
    protected String nationality; //ประกาศตัวแปร nationality เพื่อเอาไว้เก็บค่าของวัญชาติลูกค้าที่จะมาใช้บริการ 
    protected int price; //ประกาศตัวแปร price เพื่อเอาไว้เก็บค่าของราคารอยสัก
    protected int income; //ประกาศตัวแปร income เพื่อเอาไว้กับค่าของรายได้
    protected int selectartist; //ประกาศตัวแปร selectartis เพื่อเอาไว้กับค่าที่เอาไว้เลือกช่างสัก
   
    public Artist(int selectartist){ //สร้าง method ที่เป็น constructor เพื่อรับค่า selectartist มาจาก object (OVERLOAD)
        this.selectartist = selectartist; //นำค่าที่อยู่ใน selectartist ตอนสร้าง object มาไว้ใน selectartist
    }
    
    public Artist(){ //สร้าง method ที่เป็น constructor (OVERLOAD)
        this.selectartist = 1; //กำหนดค่า selectartist ให้มีค่าเท่ากับ 1
    }
    
     public boolean nationalitycheck (){ //สร้าง method nationalitycheck เพื่อเอาไว้เช็คสัญชาติของลูกค้า
        if (nationality.equals("TH")){ //ใช้คำสั่ง if เพื่อเช็คค่า nationality 
            return false; //ถ้าเงื่อนไขเป็นจริงให้ return ค่าเป็น flase
        }
        return true; //ถ้าเงื่อนไขเป็นเท็จให้ return ค่าเป็น true
        
    }
     
     void details(int size , int color,String nationality){ 
        this.nationality = nationality;
        
        if(size >= 1 && size <= 25){
            if(color == 1){
                price = 500;
                
            }
            if(color > 1){
                price = 750;
                
            }
        }
        if(size >= 26 && size <=50){
            if(color == 1){
                price = 700;
                
            }
            if(color > 1){
                price = 1050;
                
            }
        }
        if(size >= 51 && size <= 100){
            if(color == 1){
                price = 1000;
                
            }
            if(color > 1){
                price = 1500;
                
            }
        }
        if(size > 100){
            if(color == 1){
                price = 2000;
            }
            if(color > 1){
                price = 3000;
                
            }
        }
        this.income = this.income + price;
    }
}
