/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.tattoo2;

/**
 *
 * @author MSI
 */
public class ShopTattoo extends Artist{ //extends การสืบทอดMethods , ตัวแปร จากคลาสแม่มายังคลาสลูก
    
    //Overloading เหมือนกับว่า methods 2 ตัวนี้มันคล้ายกัน ติดตรงที่ว่า parameter ไม่เหมือนกัน ก็เลยเป็น Overload
    public ShopTattoo(int selectartist) { //สร้าง method ShopTattoo ที่เป็น constructor เพื่อรับค่า selectartist มาจาก object 
        super(selectartist); //ส่งค่าในตัวแปร selectartist ไปยัง Artist method ที่อยู่ใน Artist Class (คลาสแม่)

    }
    public ShopTattoo() { //สร้าง method ที่เป็น constructor 
        super(); //จะกำหนดค่าเหมือนกันกับค่าใน Artist method ที่อยู่ใน Artist Class (คลาสแม่) เพราะว่า constructor ไม่ได้กำกหนดค่ามา

    }
    
    @Override //การสร้าง method ที่มีชื่อ และก็ parameter เหมือนกันกับ method ในคลาสแม่ แต่การทำงานภายในฟังก์ชันนั้นไม่เหมือนกัน
    void details(int size, int color,String nationality) {  //สร้าง method ที่มีชื่อเหมือนกันกับ method ที่อยู่ในคลาสแม่  แต่คำสั่งการทำงานข้างในจะไม่เหมือนกัน 
        this.nationality = nationality; ////นำค่าที่อยู่ใน nationality ตอนสร้าง object มาไว้ใน nationality
        if (selectartist == 1){ //ใช้คำสั่ง if เพื่อเลือกช่างสัก
            if (nationalitycheck() == false) { //ใช้คำสั่ง if เพื่อเช็คสัญชาติ 
            if (size >= 1 && size <= 25) {
                if (color == 1) {
                    price = 500;

                }
                if (color > 1) {
                    price = 750;

                }
            }
            if (size >= 26 && size <= 50) {
                if (color == 1) {
                    price = 700;

                }
                if (color > 1) {
                    price = 1050;

                }
            }
            if (size >= 51 && size <= 100) {
                if (color == 1) {
                    price = 1000;

                }
                if (color > 1) {
                    price = 1500;

                }
            }
            if (size > 100) {
                if (color == 1) {
                    price = 2000;
                }
                if (color > 1) {
                    price = 3000;

                }
            }
            this.income = this.income + price; //ตัวแปร income มีค่าเท่ากับ ค่าที่อยู่ในตัวแปร income บวกกับ ค่าที่อยู่ในตัวแปร price
        } else {
            if (size >= 1 && size <= 25) {
                if (color == 1) {
                    price = 1000;

                }
                if (color > 1) {
                    price = 1500;

                }
            }
            if (size >= 26 && size <= 50) {
                if (color == 1) {
                    price = 1500;

                }
                if (color > 1) {
                    price = 2000;

                }
            }
            if (size >= 51 && size <= 100) {
                if (color == 1) {
                    price = 2000;

                }
                if (color > 1) {
                    price = 3000;

                }
            }
            if (size > 100) {
                if (color == 1) {
                    price = 4000;
                }
                if (color > 1) {
                    price = 5000;

                }
            }
            this.income = this.income + price; //ตัวแปร income มีค่าเท่ากับ ค่าที่อยู่ในตัวแปร income บวกกับ ค่าที่อยู่ในตัวแปร price
        }
            
        }else if(selectartist == 2){
            if (nationalitycheck() == false) {
            if (size >= 1 && size <= 25) {
                if (color == 1) {
                    price = 1000;

                }
                if (color > 1) {
                    price = 1500;

                }
            }
            if (size >= 26 && size <= 50) {
                if (color == 1) {
                    price = 2000;

                }
                if (color > 1) {
                    price = 3500;

                }
            }
            if (size >= 51 && size <= 100) {
                if (color == 1) {
                    price = 3000;

                }
                if (color > 1) {
                    price = 5000;

                }
            }
            if (size > 100) {
                if (color == 1) {
                    price = 5000;
                }
                if (color > 1) {
                    price = 8000;

                }
            }
            this.income = this.income + price; //ตัวแปร income มีค่าเท่ากับ ค่าที่อยู่ในตัวแปร income บวกกับ ค่าที่อยู่ในตัวแปร price
            
            
        } else {
            if (size >= 1 && size <= 25) {
                if (color == 1) {
                    price = 2000;

                }
                if (color > 1) {
                    price = 4000;

                }
            }
            if (size >= 26 && size <= 50) {
                if (color == 1) {
                    price = 5000;

                }
                if (color > 1) {
                    price = 8000;

                }
            }
            if (size >= 51 && size <= 100) {
                if (color == 1) {
                    price = 1000;

                }
                if (color > 1) {
                    price = 15000;

                }
            }
            if (size > 100) {
                if (color == 1) {
                    price = 20000;
                }
                if (color > 1) {
                    price = 30000;

                }
            }
            this.income = this.income + price; //ตัวแปร income มีค่าเท่ากับ ค่าที่อยู่ในตัวแปร income บวกกับ ค่าที่อยู่ในตัวแปร price
        }
        }
    }    
}
